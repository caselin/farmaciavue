$(function(){
    getProductos();
    getventas();
});

var url = 'http://api.test/productsController.php';

function getProductos(){
    $('#contenido').empty();
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(respuesta){
            var productos = respuesta;
            if(productos.length > 0){
                jQuery.each(productos, function(i, prod){
                    var btnEditar = '<button class="btn btn-success openModal" data-id="'+prod.id+'" data-Nombre="'+prod.Nombre+'" data-Laboratorio="'+prod.Laboratorio+'" data-Presentacion="'+prod.Presentacion+'" data-Via="'+prod.Via+'" data-Precio="'+prod.Precio+'" data-Cantidad="'+prod.cantidad+'" data-op="2" data-bs-toggle="modal" data-bs-target="#modalProductos"><i class="fa-solid fa-pen"></i></button>';
                    var btnEliminar = '<button class="btn btn-danger delete" data-Id="'+prod.id+'" data-Nombre="'+prod.Nombre+'"><i class="fa-solid fa-trash"></i></button>';
                    
                    $('#contenido').append('<tr><td>'+(i+1)+'</td><td>'+prod.Nombre+'</td><td>'+prod.Laboratorio+'</td><td>'+prod.Presentacion+'</td><td>'+prod.Via+'</td><td>'+'$ '+prod.Precio+'</td><td>'+prod.cantidad+' pzas'+'</td><td>'+btnEditar+' '+btnEliminar+' '+'</td></tr>');
                });
            }
        },
        error: function(){
            show_alerta('Error al mostrar los datos','error');
        }
    });
}


function getventas(){
    $('#contenidoventa').empty();
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function(respuesta){
            var productos = respuesta;
            if(productos.length > 0){
                jQuery.each(productos, function(i, prod){
                    $('#contenidoventa').append('<tr><td>'+prod.id+'</td><td>'+prod.Nombre+'</td><td>'+prod.Laboratorio+'</td><td>'+prod.Presentacion+'</td><td>'+prod.Via+'</td><td>'+'$'+prod.Precio+'</td>');
                });
            }
        },
        error: function(){
            show_alerta('Error al mostrar los datos','error');
        }
    });
}


$(document).on('click', '#btnGuardar', function(){
	var Id = $('#Id').val();
	var Nombre = $('#Nombre').val().trim();
	var Laboratorio = $('#Laboratorio').val().trim();
    var Presentacion = $('#Presentacion').val().trim();
    var Via = $('#Via').val().trim();
    var Precio = $('#Precio').val().trim();
    var cantidad = $('#cantidad').val().trim();
	var opcion = $('#btnGuardar').attr('data-operacion');
	var metodo, parametros;
	if(opcion == '1'){
		metodo="POST";
		parametros={Nombre:Nombre, Laboratorio:Laboratorio, Presentacion:Presentacion, Via:Via, Precio:Precio, cantidad:cantidad};

	}
	else{
		metodo="PUT";
		parametros={id:Id, Nombre:Nombre, Laboratorio:Laboratorio, Presentacion:Presentacion, Via:Via, Precio:Precio, cantidad:cantidad};
	}
	if(Nombre === ''){
		show_alerta('Escribe el Nombre del medicamento', 'warning', 'Nombre');
	}

	else if(Laboratorio === ''){
		show_alerta('Escribe el Laboratorio', "warning", 'Laboratorio');

	}
	else if(Presentacion==='' ){
		show_alerta('Escribe la presentacion', 'warning', 'Presentacion');
	}
    else if(Via==='' ){
		show_alerta('Escribe la via de administacion', 'warning', 'Via');
	}
    else if(Precio==='' ){
		show_alerta('Escribe el precio', 'warning', 'Precio');
	}
    else if(cantidad==='' ){
		show_alerta('Escribe la cantidad', 'warning', 'cantidad');
	}
	else{
		enviarSolicitud(metodo,parametros);
	}
});

function enviarSolicitud(metodo,parametros){
    $.ajax({
        type:metodo,
        url:url,
        data:JSON.stringify(parametros),
        dataType:'json',
        success: function(respuesta){
            show_alerta(respuesta[1],respuesta[0]);
            if(respuesta[0]=== 'success'){
                $('#btnCerrar').trigger('click');
                getProductos();
            }
        },
        error: function(){
            show_alerta('Error en la solicitud','error');
        }
    });
}

$(document).on('click','.openModal',function(){
    limpiar();
    var opcion = $(this).attr('data-op');
    if(opcion == '1'){
        $('#titulo_modal').html('Registrar Medicamento');
        $('#btnGuardar').attr('data-operacion', 1);
    } else {
        $('#titulo_modal').html('Editar Medicamento');
        $('#btnGuardar').attr('data-operacion', 2);
        var Id= $(this).attr('data-id');
        var Nombre= $(this).attr('data-Nombre');
        var Laboratorio= $(this).attr('data-Laboratorio');
        var Presentacion= $(this).attr('data-Presentacion');
        var Via= $(this).attr('data-Via');
        var Precio= $(this).attr('data-Precio');
        var cantidad= $(this).attr('data-cantidad');
        $('#Id').val(Id);
        $('#Nombre').val(Nombre);
        $('#Laboratorio').val(Laboratorio);
        $('#Presentacion').val(Presentacion);
        $('#Via').val(Via);
        $('#Precio').val(Precio);
        $('#cantidad').val(cantidad);
    }
    window.setTimeout(function(){
        $('#Nombre').trigger('focus');
    }, 500);
});


$(document).on('click', '.delete', function(){
    var Id = $(this).attr('data-Id');
    var Nombre = $(this).attr('data-Nombre');
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: { confirmButton: 'btn btn-success ms-3', cancelButton: 'btn btn-danger' },
        buttonsStyling: false
    });
    swalWithBootstrapButtons.fire({
        title: '¿Esta seguro que desea eliminar este elemento? ',
        text: 'Se perderá la información permanentemente',
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'Cancelar',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            enviarSolicitud('DELETE', { id: Id });
        } else {
            show_alerta('El elemento NO fue eliminada', 'error');
        }
    });
});


function limpiar(){
    $('#Id').val('');
    $('#Nombre').val('');
    $('#Laboratorio').val('');
    $('#Presentacion').val('');
    $('#Via').val('');
    $('#Precio').val('')
    $('#cantidad').val('')
}

function show_alerta(mensaje, icono, foco){
    if(foco !== ""){
        $('#'+foco).trigger('focus');
    }
    Swal.fire({
        title: mensaje,
        icon: icono,
        customClass: {confirmButton: 'btn btn-primary', popup: 'animated xoomIN'},
        buttonsStyling: false
    });
}

var total = 0;

$(document).on('click', '#agregarCarrito', function(){
    var nombreInput = $('#nombreInput').val();
    var cantidadInput = $('#ventacanti').val();
    var precioInput = $('#precioInput').val();
    
    
    if(nombreInput.trim() === ''){
        show_alerta('Ingrese un nombre de medicamento válido', 'warning', 'nombreInput');
        return;
    }

    if(cantidadInput <= 0 || isNaN(cantidadInput)){
        show_alerta('Ingrese una cantidad válida', 'warning', 'ventacanti');
        return;
    }

    if(precioInput <= 0 || isNaN(precioInput)){
        show_alerta('Ingrese un precio válido', 'warning', 'precioInput');
        return;
    }

    
    var producto = {
        nombre: nombreInput,
        cantidad: cantidadInput,
        precio: precioInput
    };

    
    let subtotal = producto.cantidad * producto.precio;
    $('#tablaBody').append('<tr><td>'+producto.nombre+'</td><td>'+producto.cantidad+'</td><td>'+'$'+subtotal+'</td></tr>');

    
    total += subtotal;
    $('#total').text(total.toFixed(2));

    
    $('#nombreInput').val('');
    $('#ventacanti').val('');
    $('#precioInput').val('');
});

$(document).on('click', '#btnpagar', function(){
    show_alerta('Total de la compra: $' + total.toFixed(2), 'success', 'total').then(function() {
        
        $('#tablaBody').empty();
        
        total = 0;
        
        $('#total').text(total.toFixed(2));
    });
});
$(document).on('click', '#btnrecargar', function(){
    location.reload();
});